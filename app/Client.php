<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name', 'email', 'phone', 'address'];

    public function cotizaciones(){

        return $this->hasOne('App\Quotation');
    }
}
