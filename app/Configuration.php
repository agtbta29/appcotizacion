<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    protected $fillable = ['logo', 'company_name', 'ruc', 'telephone', 'address', 'email', 'moneda', 'itbms'];
    
}
