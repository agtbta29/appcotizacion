<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Quotation;
use PDF;
use Carbon\Carbon;
use App\Configuration;
use Illuminate\Support\Facades\DB;
use App\Events\RefreshList;
class AddProductQuotation extends Controller
{

    public function __construct(){

        //$this->middleware('permission:addproduct');
    }
    public function add($id){

        $cotizacion = Quotation::findOrFail($id);
        $productos = Product::all();
        return view('cotizaciones.add', compact('productos', 'cotizacion'));
    }

    public function store(Request $request, $id){

        
        $request->validate([
            'product_id' => 'required',
        ]);
        
        $empresa = Configuration::first();

        $producto = Product::where('id', $request->product_id)->select('price')->first();
        
        $cotizacion = Quotation::findOrFail($id);
        
        $subtotal = $cotizacion->subtotal + $producto->price;

        $itbms = $subtotal * $empresa->itbms;

        $total = $subtotal + $itbms;
        
        $cotizacion->update([
            'total'     => $total,
            'subtotal'  => $subtotal,
            'itbms'     => $itbms
            ]); 
        
        $cotizacion->products()->attach($request->product_id);
        event(new RefreshList());
        //return back();
    }

    //quitar productos de la cotizacion
    public function delete(Request $request,$id){
        
        $empresa = Configuration::first();
        //se busca la cotizacion 
        $cotizacion = Quotation::findOrFail($request->cotizacion_id);
        //se hace la resta necesaria, el producto que se quita menos el total actual
        $subtotal = $cotizacion->subtotal - $request->price;
        $itbms = $subtotal * $empresa->itbms;
        $total = $subtotal + $itbms;
        //actualizamos el campo total
        $cotizacion->update([
            'total'     => $total,
            'subtotal'  => $subtotal,
            'itbms'     => $itbms
        ]);
        //quitamos los productos de la relacion
        //$cotizacion->products()->detach($id);
        $add = DB::table('product_quotation')->select('id')->where('id', $id)->delete();
        
    }

    public function useByVue($id){
        $cotizacion = Quotation::with('products')->findOrFail($id);

        return $cotizacion;
    }

    public function getProducts(){
        
        $productos = Product::all();
        return $productos;
    }


}
