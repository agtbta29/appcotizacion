<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuration;
class ConfigController extends Controller
{
    public function index(){

        $config = Configuration::first();
        return view('configuracion.index', compact('config'));
    }

    public function edit($empresa){

        $config = Configuration::where('company_name', $empresa)->first();

        return view('configuracion.edit', compact('config'));
    }

    public function update(Request $request, $empresa){

        $config = Configuration::where('company_name', $empresa);

        $config->update([

            'logo'              =>          $request->logo, 
            'company_name'      =>          $request->company_name, 
            'ruc'               =>          $request->ruc, 
            'phone'             =>          $request->phone, 
            'address'           =>          $request->address, 
            'email'             =>          $request->email, 
            'moneda'            =>          $request->moneda, 
            'itbms'             =>          $request->itbms
        ]);
        
        return redirect()->route('configuracion.index');
    }
}
