<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ValidateNewProduct;
use App\Http\Requests\ValidateUpdateProduct;
use Illuminate\Support\Facades\DB;
use App\Product;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productos = Product::all();
        return view('productos.index', compact('productos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('productos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ValidateNewProduct $request)
    {
        //si el campo file no esta vacio
        if ($request->hasFile('file')) {
            //se obtiene el nombre original del archivo
            $nombre = $request->file('file')->getClientOriginalName();
            //se guarda en el directorio publico
            $path = $request->file('file')->storeAs('public', $nombre);
          }
          else{
              $path = '';
          }

          Product::create([

            'file'      =>      $path,
            'name'      =>      $request->name,
            'cod'      =>       $request->cod,
            'stock'     =>      $request->stock,
            'price'     =>      $request->price,

          ]);

          return back()->with('save', 'Producto guardado correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $producto =  Product::findOrFail($id);

       return view('productos.edit', compact('producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ValidateUpdateProduct $request, $id)
    {
        
        
        
        //si el campo file no esta vacio
        if ($request->hasFile('file')) {
            //se obtiene el nombre original del archivo
            $nombre = $request->file('file')->getClientOriginalName();
            //se guarda en el directorio publico
            $path = $request->file('file')->storeAs('public', $nombre);
          }
          else{
              $pic = DB::table('products')->where('id', $id)->select('file')->get();
              $path = $pic->implode('file');
              
          }

          Product::findOrFail($id)->update([

            'file'      =>      $path,
            'name'      =>      $request->name,
            'cod'      =>       $request->cod,
            'stock'     =>      $request->stock,
            'price'     =>      $request->price,

          ]);

          return back()->with('save', 'Producto actualizado correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::findOrfail($id)->delete();

        return back()->with('delete', 'Producto eliminado de la base de datos');
    }
}
