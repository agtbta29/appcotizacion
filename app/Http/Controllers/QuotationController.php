<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Quotation;
use Carbon\Carbon;
use PDF;
use App\Configuration;

class QuotationController extends Controller
{

    public function __construct(){

       $this->middleware('permission:cotizaciones.index')->only('index');
       $this->middleware('permission:cotizaciones.create')->only(['create',  'store']);
       $this->middleware('permission:cotizaciones.edit')->only(['edit','update']);
       $this->middleware('permission:cotizaciones.destroy')->only('destroy');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cotizaciones = Quotation::with('cliente')->get();
        return view('cotizaciones.index', compact('cotizaciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Client::all();
        return view('cotizaciones.create', compact('clientes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'client_id'         =>      'required',
            'waytopay'          =>      'required',
            'fecha_vencimiento' =>      'required'
        ]);

        Quotation::create([
            'user_id'               =>      auth()->user()->id,
            'client_id'             =>      $request->client_id,
            'waytopay'              =>      $request->waytopay,
            'fecha_vencimiento'     =>      $request->fecha_vencimiento,
        ]);

        return back()->with('save', 'Cotizacion agregada correctamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = Configuration::first();

        $cotizacion = Quotation::findOrFail($id);

        $pdf = PDF::loadView('cotizaciones.pdf', compact('cotizacion', 'empresa'));
  
        return $pdf->stream();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'waytopay'          =>      'required',
            'fecha_vencimiento' =>      'required'
        ]);

        $cotizacion = Quotation::findOrfail($id);

        $cotizacion->update([

            'waytopay'              =>  $request->waytopay,
            'fecha_vencimiento'     =>  $request->fecha_vencimiento,
        ]);

        return redirect('agregarproductos/'.$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cotizacion = Quotation::findOrfail($id);
        $cotizacion->delete();
        $cotizacion->products()->detach($id);
        return back();
    }
}
