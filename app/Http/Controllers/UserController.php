<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
class UserController extends Controller
{
    public function index(){

        
        //mostrar usuarios excepto el actualmente autenticado
        $usuarios = User::where('id', '!=', auth()->user()->id)->get();

        return view('usuarios.index', compact('usuarios'));
    }

    public function edit($id){
        
        $usuario = User::findOrFail($id);

        return view('usuarios.edit', compact('usuario'));
    }

    public function update(Request $request, $id){

        User::findOrFail($id)->update([
            'name'     =>   $request->name,
            'email'    =>   $request->email
        ]);

        return redirect()->route('usuarios.index');
    }

    public function editPass($id){

        $usuario = User::findOrFail($id);

        return view('usuarios.password', compact('usuario'));
    }

    public function updatePass(Request $request, $id){

        $request->validate([
            'password'      =>      'required|confirmed'
        ]);

        User::findOrFail($id)->fill([
            'password' => Hash::make($request->password)
        ])->save();

        return redirect()->route('usuarios.index');
    }

    public function delete($id){

        User::findOrFail($id)->delete();

        return back();
    }

    public function perfil($id){

        $usuario = User::findOrFail($id);

        return view('usuarios.perfil', compact('usuario'));
    }

    public function create(){

        $roles = Role::all();
        return view('usuarios.create', compact('roles'));
    }

    public function store(Request $request){

        
        $request->validate([

            'name'      =>      'required',
            'email'     =>      'required|unique:users,email',
            'password'  =>      'required|confirmed'
        ]);

        $usuario = User::create([

            'name'      =>      $request->name,
            'email'     =>      $request->email,
            'password'  =>      Hash::make($request->password) 
        ]);
        
        $usuario->assignRole($request->role_id);
        return redirect()->route('usuarios.index');
    }
}
