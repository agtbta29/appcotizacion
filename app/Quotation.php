<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $fillable = ['user_id','client_id', 'waytopay', 'fecha_vencimiento', 'subtotal', 'itbms', 'total'];

    protected $dates = ['fecha_vencimiento'];

    public function cliente(){

        return $this->belongsTo('App\Client', 'client_id');
    }

    public function products(){

        return $this->belongsToMany('App\Product')->withPivot('id');
    }

    public function seller(){

        return $this->belongsTo('App\User', 'user_id');
    }
}
