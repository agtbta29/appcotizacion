<?php

use Illuminate\Database\Seeder;
use App\Configuration;
class ConfigurationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Configuration::create([

            'company_name'      =>      'laravel',
            'ruc'               =>      '00000000000',
            'phone'             =>      '000-0000',
            'address'           =>      'Panamá',
            'email'             =>      'laravel@gmail.com',
            'moneda'            =>      'B/.',
            'itbms'             =>      '0'
        ]);
    }
}
