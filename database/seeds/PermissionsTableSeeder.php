<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\User;
class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //permisos password
        Permission::create([
            'name'          => 'edit.password',
            'description'   => 'editar Contraseña'
            ]);

        //permisos usuarios
        Permission::create([
            'name'          =>      'usuarios.index',
            'description'   =>      'ver usuarios del sistema'
            ]);

        Permission::create([
            'name'         =>       'usuarios.store',
            'description'  =>       'crear usuarios'
            ]);

        Permission::create([
            'name'              =>      'usuarios.edit',
            'description'       =>      'editar usuario'
            ]);

        //permisos de configuración
        Permission::create([
            'name' => 'configuracion.index',
            'description'       =>      'ver configuración del sistema'
            ]);

        Permission::create([
            'name'              => 'configuracion.update',
            'description'       => 'modificar configuración'
            ]);

        //permisos para agregar productos a una cotizacion
        Permission::create([
            'name'              => 'addproduct',
            'description'       => 'agregar productos a una cotización'
            ]);

        //permiso clientes
        Permission::create(['name' => 'clientes.index', 'description' => 'ver clientes']);
        Permission::create(['name' => 'clientes.create', 'description' => 'crear clientes']);
        Permission::create(['name' => 'clientes.show', 'description' => 'ver Información de un cliente']);
        Permission::create(['name' => 'clientes.edit', 'description' => 'editar clientes']);
        Permission::create(['name' => 'clientes.destroy', 'description' => 'eliminar clientes']);

        //permiso productos
        Permission::create(['name' => 'productos.index', 'description' => 'ver productos']);
        Permission::create(['name' => 'productos.create', 'description' => 'crear productos']);
        Permission::create(['name' => 'productos.show', 'description' => 'ver información de un producto']); 
        Permission::create(['name' => 'productos.edit', 'description' => 'editar productos']);
        Permission::create(['name' => 'productos.destroy', 'description' => 'eliminar productos']);//permiso para eliminar 

        //permiso cotizaciones
        Permission::create(['name' => 'cotizaciones.index', 'description' => 'ver cotizaciones']);
        Permission::create(['name' => 'cotizaciones.create', 'description' => 'crear cotizaciones']);
        Permission::create(['name' => 'cotizaciones.show', 'description' => 'ver información de una cotización']);//permiso para ver perfil de los 
        Permission::create(['name' => 'cotizaciones.edit', 'description' => 'editar cotizaciones']);
        Permission::create(['name' => 'cotizaciones.destroy', 'description' => 'eliminar cotizaciones']);//permiso para eliminar 

        //Admin
        $admin = Role::create(['name' => 'administrador']);
        $admin->givePermissionTo(Permission::all());

        //asignar role administrador al usuario por defecto
        $user = User::find(1); //Italo Morales
        $user->assignRole('administrador');
    }
}
