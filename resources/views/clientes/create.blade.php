@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                            <a href="{{route('clientes.index')}}" class="btn btn-primary btn-sm">
                                <i class="fa fa-arrow-left" aria-hidden="true"></i>
                            </a>
                        Nuevo Cliente
                    </div>
                </div>
                
                <div class="card-body">
                        <form class="form-horizontal" method="POST" action="{{route('clientes.store')}}">
                            @csrf
                                <fieldset>
                                
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="textinput">Nombre</label>  
                                  <div class="col-md-12">
                                  <input id="textinput" name="name" type="text" placeholder="" class="form-control input-md">
                                  {!!$errors->first('name', '<b class="text-danger">:message</b>')!!}
                                  </div>
                                </div>
                                
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="email">Email</label>  
                                  <div class="col-md-12">
                                  <input id="email" name="email" type="email" placeholder="" class="form-control input-md">
                                  {!!$errors->first('email', '<b class="text-danger">:message</b>')!!}
                                  </div>
                                </div>
                                
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="phone">Teléfono</label>  
                                  <div class="col-md-12">
                                  <input id="phone" name="phone" type="text" placeholder="" class="form-control input-md">
                                  {!!$errors->first('phone', '<b class="text-danger">:message</b>')!!}
                                  </div>
                                </div>
                                
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="address">Dirección</label>  
                                  <div class="col-md-12">
                                  <input id="address" name="address" type="text" placeholder="" class="form-control input-md">
                                  {!!$errors->first('address', '<b class="text-danger">:message</b>')!!}
                                  </div>
                                </div>
                                
                                <!-- Button -->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for=""></label>
                                  <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary">Guardar</button>
                                  </div>
                                </div>
                                
                                </fieldset>
                                </form>
                                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection