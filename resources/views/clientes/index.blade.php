@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                        Clientes
                        <a href="{{route('clientes.create')}}" class="btn btn-primary btn-sm float-right">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Correo Electrónico</th>
                                <th>Teléfono</th>
                                <th>Dirección</th>
                                <th><center>Editar</center></th>
                                <th><center>Eliminar</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($clientes as $cliente)
                                <tr>
                                    <td>{{$cliente->id}}</td>
                                    <td>{{$cliente->name}}</td>
                                    <td>{{$cliente->email}}</td>
                                    <td>{{$cliente->phone}}</td>
                                    <td>{{$cliente->address}}</td>
                                    <td>
                                        <center>
                                        <a href="{{route('clientes.edit', $cliente->id)}}" class="btn btn-warning btn-sm">
                                            <i class="fa fa-pencil" aria-hidden="true"></i>
                                        </a>
                                        </center>
                                    </td>
                                    <td>
                                        <center>
                                        <form action="{{route('clientes.destroy', $cliente->id)}}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('¿Estas Seguro de eliminar el cliente?')">
                                                    <i class="fa fa-window-close" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                        </center>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection