@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                        Configuración de Sistema
                    </div>
                </div>
                
                <div class="card-body">
                    <form action="{{route('configuracion.update', $config->company_name)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Nombre De la Compañía</th>
                                    <td>
                                        <input type="text" class="form-control input-md" name="company_name" value="{{$config->company_name}}" autofocusñññ>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Logo</th>
                                    <td><img src="" alt=""></td>
                                </tr>
                                <tr>
                                    <th>RUC</th>
                                    <td>
                                        <input type="text" class="form-control input-md" name="ruc" value="{{$config->ruc}}">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Teléfono</th>
                                    <td>
                                        <input type="text"  class="form-control input-md" name="phone" value="{{$config->phone}}">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Dirección</th>
                                    <td>
                                        <input type="text"  class="form-control input-md" name="address" value="{{$config->address}}">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td>
                                        <input type="email"  class="form-control input-md" name="email" value="{{$config->email}}">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Moneda</th>
                                    <td>
                                        <input type="text"   class="form-control input-md" name="moneda" value="{{$config->moneda}}">
                                    </td>
                                </tr>
                                <tr>
                                    <th>ITBMS</th>
                                    <td>
                                        <input type="text"  class="form-control input-md" name="itbms" value="{{$config->itbms}}">
                                    </td>
                                </tr>
                            </thead>
                        </table>
                        <button type="submit" class="btn btn-primary btn-sm">Guardar Cambios</button>
                    </form>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection