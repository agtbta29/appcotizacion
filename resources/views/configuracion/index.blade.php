@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                        Configuración de Sistema
                    </div>
                </div>
                
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre De la Compañía</th>
                                <td>{{$config->company_name}}</td>
                            </tr>
                            <tr>
                                <th>Logo</th>
                                <td><img src="" alt=""></td>
                            </tr>
                            <tr>
                                <th>RUC</th>
                                <td>{{$config->ruc}}</td>
                            </tr>
                            <tr>
                                <th>Teléfono</th>
                                <td>{{$config->telephone}}</td>
                            </tr>
                            <tr>
                                <th>Dirección</th>
                                <td>{{$config->address}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$config->email}}</td>
                            </tr>
                            <tr>
                                <th>Moneda</th>
                                <td>{{$config->moneda}}</td>
                            </tr>
                            <tr>
                                <th>ITBMS</th>
                                <td>{{$config->itbms}}</td>
                            </tr>
                        </thead>
                    </table>
                    </div>
                    <a href="{{route('configuracion.edit', $config->company_name)}}" class="btn btn-primary btn-sm">Cambiar Configuración</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection