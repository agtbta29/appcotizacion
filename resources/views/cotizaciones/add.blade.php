
@extends('layouts.app')

@section('content')

<div class="container">
    @php
        use App\Http\Controllers\RecursosAccesibles;
        $balboa =  RecursosAccesibles::getData();
    @endphp
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                            <a href="{{route('cotizaciones.index')}}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                </a>
                        Cliente: {{$cotizacion->cliente->name}}
                    </div>
                </div>
                
                <div class="card-body">
                   <form method="POST" action="{{route('cotizaciones.update', $cotizacion->id)}}">
                    @csrf
                    @method('PUT')
                        <fieldset>
                    
                
                <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Forma de Pago</label>  
                        <div class="col-md-12">
                                <select id="c" name="waytopay" class="form-control input-md" {{auth()->user()->can('cotizaciones.edit') ? '' : 'disabled'}}>
                                        <option value="">Selecciona una forma de pago</option>
                                        <option value="efectivo" {{$cotizacion->waytopay === 'efectivo' ? 'selected' : ''}}>Efectivo</option>
                                        <option value="tarjeta de credito" {{$cotizacion->waytopay === 'tarjeta de credito' ? 'selected' : ''}}>Tarjeta de Crédito</option>
                                        <option value="cheque" {{$cotizacion->waytopay === 'cheque' ? 'selected' : ''}}>Cheque</option>
                                        
                                </select>
                        
                        </div>
                    </div>
                    <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Fecha de Vencimiento</label>  
                            <div class="col-md-12">
                                <input type="date" class="form-control input-md" name="fecha_vencimiento" value="{{$cotizacion->fecha_vencimiento->format('Y-m-d')}}" {{auth()->user()->can('cotizaciones.edit') ? '' : 'disabled'}}>
                            </div>
                        </div>
    
                   @can('cotizaciones.edit')
                        <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-12">
                          <button type="submit" class="btn btn-primary">Modificar</button>
                        </div>
                      </div>
                   @endcan
                        </fieldset>
                   </form>
                </div>
            </div>
        </div>
    </div>
</div>
<products-quotation id="{{$cotizacion->id}}"></products-quotation>
<add-products-quotation id="{{$cotizacion->id}}"></add-products-quotation>
{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                        Productos Agregados
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-sm">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Código</th>
                                <th>Precio</th>
                                <th>Quitar</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cotizacion->products as $item)
                                <tr>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->cod}}</td>
                                    <td>{{$balboa->moneda}}{{$item->price}}</td>
                                    <td>
                                      <form action="{{route('deleteproduct', $item->pivot->id)}}" method="POST">
                                        @csrf
                                        <input type="text" name="cotizacion_id" value="{{$cotizacion->id}}" hidden>
                                        <input type="text" name="price" value="{{$item->price}}" hidden>
                                        <button class="btn btn-danger btn-sm" type="submit">
                                            
                                            <i class="fa fa-window-close" aria-hidden="true"></i>
                                        </button>
                                      </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                    <label for="">Sub-Total</label>({{$balboa->moneda}})
                    <input type="text" value="{{$cotizacion->subtotal}}" disabled>
                    <label for="">ITBMS</label>({{$balboa->moneda}})
                    <input type="text" value="{{$cotizacion->itbms}}" disabled>
                    <label for="">Total</label>({{$balboa->moneda}})
                    <input type="text" value="{{$cotizacion->total}}" disabled>
                    
                </div>
              
            </div>
        </div>
    </div>
</div>--}}


{{--<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                        Agregar Productos
                    </div>
                </div>
                
                <div class="card-body">
                    <form action="{{route('storeproduct', $cotizacion->id)}}" method="POST">
                        @csrf
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Productos</label>  
                            <div class="col-md-12">
                                    <select id="addproduct" name="product_id" class="form-control input-md">
                                            <option value="">Buscar Productos</option>
                                            @foreach ($productos as $producto)
                                            <option value="{{$producto->id}}">{{$producto->name}}</option>
                                            @endforeach
                                    </select>
                                    {!!$errors->first('product_id', '<b class="text-danger">:message</b>')!!}
                            </div>
                            
                        </div>
                        
                        <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-12">
                          <button type="submit" class="btn btn-primary">Agregar</button>
                        </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
--}}
@endsection
