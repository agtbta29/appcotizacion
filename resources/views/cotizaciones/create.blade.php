@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                            <a href="{{route('cotizaciones.index')}}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                </a>
                        Nueva Cotización
                    </div>
                </div>
                <div class="card-body">
                <form class="form-horizontal" method="POST" action="{{route('cotizaciones.store')}}">
                    @csrf
                    <fieldset>
                        
                    
                    <!-- Text input-->
            <div class="form-group">
                <label class="col-md-4 control-label" for="textinput">Cliente</label>  
                <div class="col-md-12">
                        <select id="c" name="client_id" class="form-control input-md">
                                <option value="">Selecciona un cliente</option>
                                @foreach ($clientes as $cliente)
                                <option value="{{$cliente->id}}">{{$cliente->name}}</option>
                                @endforeach
                        </select>
                
                </div>
            </div>
            <div class="form-group">
                    <label class="col-md-4 control-label" for="textinput">Forma de Pago</label>  
                    <div class="col-md-12">
                            <select id="c" name="waytopay" class="form-control input-md">
                                    <option value="">Selecciona una forma de pago</option>
                                    <option value="efectivo">Efectivo</option>
                                    <option value="tarjeta de credito">Tarjeta de Crédito</option>
                                    <option value="cheque">Cheque</option>
                                    
                            </select>
                    
                    </div>
                </div>
                <div class="form-group">
                        <label class="col-md-4 control-label" for="textinput">Fecha de Vencimiento</label>  
                        <div class="col-md-12">
                            <input type="date" class="form-control input-md" name="fecha_vencimiento">
                        </div>
                    </div>

                <!-- Button -->
                <div class="form-group">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-12">
                          <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                      </div>
                    </fieldset>
                </form>
                                
                </div>   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
