@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                        Cotizaciones
                        @can('cotizaciones.create')
                            <a href="{{route('cotizaciones.create')}}" class="btn btn-primary btn-sm float-right">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                            </a>
                        @endcan
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table table-hover table-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Cliente</th>
                                <th>Vendedor</th>
                                <th>Forma de Pago</th>
                                <th>Cantidad</th>
                                <th>Total</th>
                                <th>Creación</th>
                                <th>Vencimiento</th>
                                @can('addproduct')
                                <th><center>Agregar/Quitar</center></th>
                                @endcan
                                @can('cotizaciones.show')
                                <th><center>Ver/Dercargar</center></th>
                                @endcan
                                @can('cotizaciones.destroy')
                                <th><center>Eliminar</center></th>
                                @endcan
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($cotizaciones as $cotizacion)
                                <tr>
                                    <td>{{$cotizacion->id}}</td>
                                    <td>{{$cotizacion->cliente->name}}</td>
                                    <td>{{$cotizacion->seller->name}}</td>
                                    <td>{{$cotizacion->waytopay}}</td>
                                    <td><center>{{$cotizacion->products->count()}}</center></td>
                                    <td>{{$cotizacion->total}}</td>
                                    <td>{{$cotizacion->created_at->format('d-m-Y')}}</td>
                                    <td>{{$cotizacion->fecha_vencimiento->format('d-m-Y')}}</td>
                                    @can('addproduct')
                                    <td>
                                        <center>
                                            <a href="{{route('addproduct', $cotizacion->id)}}" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                                            </a>
                                        </center>
                                    </td>
                                    @endcan
                                    @can('cotizaciones.show')
                                    <td>
                                        <center>
                                            <a href="{{route('cotizaciones.show', $cotizacion->id)}}" target="_blank" class="btn btn-primary btn-sm">
                                                    <i class="fa fa-download" aria-hidden="true"></i>
                                            </a>
                                        </center>
                                    </td>
                                    @endcan
                                    @can('cotizaciones.destroy')
                                    <td>
                                        <center>
                                            <form action="{{route('cotizaciones.destroy', $cotizacion->id)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('¿Estas Seguro de eliminar el cliente?')">
                                                        <i class="fa fa-window-close" aria-hidden="true"></i>
                                                </button>
                                            </form>
                                            </center>
                                    </td>
                                    @endcan
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
