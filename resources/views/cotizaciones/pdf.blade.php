
<!DOCTYPE html>
<html>
<head>
	<title>Cotización</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
	<style>
		.cuadro{
			width: 100%;
			border: 1px solid black;
			margin-bottom: 1%;
			padding: 10px;
			border-radius: 5px;
		}
	</style>
</head>
<body>
		<div class="container->fluid">
				<div class="row">
						<div class="col-sm-12">
							<strong><center><h2>{{$empresa->company_name}}</h2></center></strong>
							<label class="text-secondary"><center>R.U.C. {{$empresa->ruc}}</center></label>
						</div>
						
				</div>
				<div class="cuadro">
						<p><strong>Teléfono: </strong>{{$empresa->telephone}}</p>
						<p><strong>Dirección: </strong>{{$empresa->address}}</p>
						<p><strong>Email: </strong>{{$empresa->email}}</p>
				</div>
		</div>
	
		<div class="row">
			<div class="col-sm-12">
				<table class="table table-sm table-bordered">
					<thead>
						<tr class="table-primary">
							<th>Cliente</th>
							<th>Forma de Pago</th>
							<th>Fecha</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td rowspan="3">
								<p>{{$cotizacion->cliente->name}}</p>
							</td>
							<td>{{$cotizacion->waytopay}}</td>
							<td> {{$cotizacion->created_at->format('M-d-Y')}}</td>
						</tr>
						<tr class="table-primary">
							<th>Vendedor</th>
							<th>Vencimiento</th>
						</tr>
						<tr>
							<td>{{$cotizacion->seller->name}}</td>
							<td>{{$cotizacion->fecha_vencimiento->format('M-d-Y')}}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<!--otra fila-->
		<div class="row">
			<div class="col-sm-12">
				<table class="table table-striped table-sm">
					<thead>
						<tr>
							<th>Producto</th>
							<th>Código</th>
							<th>Precio</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($cotizacion->products as $item)
						<tr>
							<td>{{$item->name}}</td>
							<td>{{$item->cod}}</td>	
							<td>{{$empresa->moneda}}{{$item->price}}</td>
						</tr>	
						@endforeach
						<tr>
							<th></th>
							<th>Sub-Total</th>
							<td>{{$empresa->moneda}}{{$cotizacion->subtotal}}</td>
						</tr>
						<tr>
								<th></th>
								<th>ITBMS</th>
								<td>{{$empresa->moneda}}{{$cotizacion->itbms}}</td>
							</tr>
						<tr>
								<th></th>
								<th>Total</th>
								<td>{{$empresa->moneda}}{{$cotizacion->total}}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</body>
</html>