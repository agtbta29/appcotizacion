@php
        use App\Http\Controllers\RecursosAccesibles;
        $e =  RecursosAccesibles::getData();
@endphp

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title></title>

    

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
   
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-dark navbar-laravel sticky-top">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ $e->company_name }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            
                        @else
                        <!--Enlaces de navegacion-->
                        @can('clientes.index')
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('clientes*') ? 'active' : ''}}" href="{{route('clientes.index')}}">Clientes 
                                <i class="fa fa-handshake-o" aria-hidden="true"></i>
                            </a>
                        </li>
                        @endcan
                        @can('productos.index')
                        <li class="nav-item">
                                <a class="nav-link {{request()->is('productos*') ? 'active' : ''}}" href="{{route('productos.index')}}">Productos
                                        <i class="fa fa-cubes" aria-hidden="true"></i>
                                </a>
                        </li>
                        @endcan
                        @can('cotizaciones.index')
                        <li class="nav-item">
                            <a class="nav-link {{request()->is('cotizaciones*') ? 'active' : ''}}" href="{{route('cotizaciones.index')}}">Cotizaciones 
                                <i class="fa fa-book" aria-hidden="true"></i>
                            </a>
                        </li>
                        @endcan
                        @can('configuracion.index')
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle {{request()->is('configuracion*') ? 'active' : ''}}" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>Configuración</a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

                            <a class="dropdown-item" href="{{route('configuracion.index')}}">Empresa</a>

                            <a class="dropdown-item" href="{{route('usuarios.index')}}">Usuarios</a>

                            <a class="dropdown-item" href="{{route('roles.index')}}">Roles</a>

                            </div>
                        </li>
                        @endcan
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
    
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    
                                    <a class="dropdown-item" href="{{route('perfil', auth()->user()->id)}}">Mi Perfíl</a>

                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Cerrar Sesión') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            
                            <!--Enlaces de navegacion-->
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script src="{{asset('js/app.js')}}"></script>
    
    <!--si se guardó correctamente-->
    @if (session('save'))
        <script>
            toastr.success('{{ session('save') }}')
        </script>
    <!--si se mofificó correctamente-->
    @elseif(session('update'))
        <script>
            toastr.success('{{ session('update') }}')
        </script>
    @elseif(session('delete'))
    <script>
        toastr.error('{{ session('delete') }}')
    </script>
    @endif
    <script>
           
        $(document).ready(function() {
            $('#c').select2();
        });
    </script>
    <script>
           
        $(document).ready(function() {
            $('#addproduct').select2();
        });
    </script>
    <script src="https://unpkg.com/vue@latest"></script>
    <script src="https://unpkg.com/vue-select@latest"></script>
</body>
</html>
