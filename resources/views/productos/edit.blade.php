@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                            <a href="{{route('productos.index')}}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                            </a>
                        Editar Producto
                    </div>
                </div>
                
                <div class="card-body">
                        <form class="form-horizontal" method="POST" action="{{route('productos.update', $producto->id)}}" enctype="multipart/form-data">
                               @csrf
                               @method('PUT')
                                <!-- File Button --> 
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="file">Imagen</label>
                                  <div class="col-md-4">
                                    <input id="file" name="file" class="input-file" type="file" value="{{$producto->file}}"><hr>
                                    <img src="{{Storage::url($producto->file)}}" width="150px" height="150px">
                                  </div>
                                 
                                </div>
                                
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="name">Nombre</label>  
                                  <div class="col-md-12">
                                  <input id="name" name="name" type="text" placeholder="" class="form-control input-md" value="{{$producto->name}}">
                                  {!!$errors->first('name', '<b class="text-danger">:message<b>')!!}
                                  </div>
                                </div>
                                
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="code">Código</label>  
                                  <div class="col-md-12">
                                  <input id="code" name="cod" type="text" placeholder="" class="form-control input-md" value="{{$producto->cod}}">
                                  {!!$errors->first('cod', '<b class="text-danger">:message<b>')!!}
                                  </div>
                                </div>
                                
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="stock">Cantidad</label>  
                                  <div class="col-md-12">
                                  <input id="stock" name="stock" type="text" placeholder="" class="form-control input-md" value="{{$producto->stock}}">
                                  {!!$errors->first('stock', '<b class="text-danger">:message<b>')!!}
                                  </div>
                                </div>
                                
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="price">Precio</label>  
                                  <div class="col-md-12">
                                  <input id="price" name="price" type="text" placeholder="" class="form-control input-md" value="{{$producto->price}}">
                                  {!!$errors->first('price', '<b class="text-danger">:message<b>')!!}
                                  </div>
                                </div>
                                
                                <!-- Button -->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for=""></label>
                                  <div class="col-md-4">
                                    <button type="submit" class="btn btn-primary">Actualizar</button>
                                  </div>
                                </div>
                                
                                </fieldset>
                                </form>
                                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection