@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                        Productos
                        <a href="{{route('productos.create')}}" class="btn btn-primary btn-sm float-right">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                
                <div class="card-body">
                    <div class="table-responsive">
                    <table class="table tabla-hover table-sm">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Imagen</th>
                                <th>Nombre</th>
                                <th>Código</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                                <th><center>Editar</center></th>
                                <th><center>Eliminar</center></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($productos as $producto)
                                <tr>
                                    <td>{{$producto->id}}</td>
                                    <td>
                                        <img src="{{Storage::url($producto->file)}}" width="50px" height="50px">
                                    </td>
                                    <td>{{$producto->name}}</td>
                                    <td>{{$producto->cod}}</td>
                                    <td>{{$producto->stock}}</td>
                                    <td>{{$producto->price}}</td>
                                    <td>
                                            <center>
                                                <a href="{{route('productos.edit', $producto->id)}}" class="btn btn-warning btn-sm">
                                                    <i class="fa fa-pencil" aria-hidden="true"></i>
                                                </a>
                                            </center>
                                    </td>
                                    <td>
                                        <center>
                                            <form action="{{route('productos.destroy', $producto->id)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('¿Estas Seguro de eliminar el producto?')">
                                                        <i class="fa fa-window-close" aria-hidden="true"></i>
                                                </button>
                                            </form>
                                            </center>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection