@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                            <a href="{{route('roles.index')}}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                                </a>
                        Editar Role
                    </div>
                </div>
                
                <div class="card-body">
                    <form action="{{route('roles.update', $role->id)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label class="col-md-4 control-label" for="textinput">Nombre</label>  
                            <div class="col-md-12">
                                    <input type="text" name="name" class="form-control input-md" value="{{$role->name}}">
                                    
                            </div>
                        </div>
                     
                        <div class="container">
                            <div class="row">
                                
                                <div class="col-sm-3">
                                        <h4>Módulo Clientes</h4>
                                    <!--Módulo Clientes-->
                                    @foreach ($permisos as $permiso)
                                        @if ($permiso->name == 'clientes.index' | 
                                                $permiso->name == 'clientes.show' | 
                                                $permiso->name == 'clientes.create'|
                                                $permiso->name == 'clientes.edit'  |
                                                $permiso->name == 'clientes.destroy')
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="permission_id[]" value="{{$permiso->id}}" id="{{$permiso->id}}" {{ $role->permissions->pluck('id')->contains($permiso->id) ? 'checked' : ''}}>
                                            <label class="form-check-label" for="{{$permiso->id}}">
                                                {{$permiso->description}}
                                            </label>
                                    </div> 
                                        @endif
                                     @endforeach
                                     
                                </div>
                                <!--Módulo Productos-->
                                <div class="col-sm-3">
                                        <h4>Módulo Productos</h4>
                                        <!--Módulo Productos-->
                                        @foreach ($permisos as $permiso)
                                            @if ($permiso->name == 'productos.index' | 
                                                    $permiso->name == 'productos.create'|
                                                    $permiso->name == 'productos.store' |
                                                    $permiso->name == 'productos.edit'  |
                                                    $permiso->name == 'productos.update'|
                                                    $permiso->name == 'productos.destroy')
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="permission_id[]" value="{{$permiso->id}}" id="{{$permiso->id}}" {{ $role->permissions->pluck('id')->contains($permiso->id) ? 'checked' : ''}}>
                                                <label class="form-check-label" for="{{$permiso->id}}">
                                                    {{$permiso->description}}
                                                </label>
                                        </div> 
                                            @endif
                                         @endforeach
                                         <!--Módulo -->
                                    </div>
                                    <!--Módulo Productos-->
                                <div class="col-sm-3">
                                        <h4>Módulo Cotizaciones</h4>
                                        <!--Módulo Cotizaciones-->
                                        @foreach ($permisos as $permiso)
                                            @if ($permiso->name == 'cotizaciones.index' | 
                                                    $permiso->name == 'cotizaciones.create'|
                                                    $permiso->name == 'cotizaciones.store' |
                                                    $permiso->name == 'cotizaciones.edit'  |
                                                    $permiso->name == 'cotizaciones.update'|
                                                    $permiso->name == 'addproduct'|
                                                    $permiso->name == 'cotizaciones.destroy')
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="permission_id[]" value="{{$permiso->id}}" id="{{$permiso->id}}" {{ $role->permissions->pluck('id')->contains($permiso->id) ? 'checked' : ''}}>
                                                <label class="form-check-label" for="{{$permiso->id}}">
                                                    {{$permiso->description}}
                                                </label>
                                        </div> 
                                            @endif
                                         @endforeach
                                         <!--Módulo -->
                                </div>
                                <div class="col-sm-3">
                                        <h4>Módulo Usuarios</h4>
                                        <!--Módulo Usuarios-->
                                        @foreach ($permisos as $permiso)
                                            @if ($permiso->name == 'usuarios.index' | 
                                                    $permiso->name == 'usuarios.create'|
                                                    $permiso->name == 'usuarios.store' |
                                                    $permiso->name == 'usuarios.edit'  |
                                                    $permiso->name == 'usuarios.update'|
                                                    $permiso->name == 'usuarios.delete')
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="permission_id[]" value="{{$permiso->id}}" id="{{$permiso->id}}" {{ $role->permissions->pluck('id')->contains($permiso->id) ? 'checked' : ''}}>
                                                <label class="form-check-label" for="{{$permiso->id}}">
                                                    {{$permiso->description}}
                                                </label>
                                        </div> 
                                            @endif
                                         @endforeach
                                         <!--Módulo -->
                                </div>
                                <hr>
                                <br>
                                
                            </div>
                        </div>   
                        <hr>
                        <div class="container">
                                <div class="row">
                                    
                                    <div class="col-sm-3">
                                            <h4>Módulo Configuración Empresa</h4>
                                        <!--Módulo Clientes-->
                                        @foreach ($permisos as $permiso)
                                            @if ($permiso->name == 'configuracion.index' | 
                                                    $permiso->name == 'configuracion.update'|
                                                    $permiso->name == 'configuracion.edit')
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="permission_id[]" value="{{$permiso->id}}" id="{{$permiso->id}}" {{ $role->permissions->pluck('id')->contains($permiso->id) ? 'checked' : ''}}>
                                                <label class="form-check-label" for="{{$permiso->id}}">
                                                    {{$permiso->description}}
                                                </label>
                                        </div> 
                                            @endif
                                         @endforeach
                                         
                                    </div>

                                    <div class="col-sm-3">
                                            <h4>Módulo Contraseñas</h4>
                                        <!--Módulo Clientes-->
                                        @foreach ($permisos as $permiso)
                                            @if ($permiso->name == 'edit.password' | 
                                                    $permiso->name == 'update.password')
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" name="permission_id[]" value="{{$permiso->id}}" id="{{$permiso->id}}" {{ $role->permissions->pluck('id')->contains($permiso->id) ? 'checked' : ''}}>
                                                <label class="form-check-label" for="{{$permiso->id}}">
                                                    {{$permiso->description}}
                                                </label>
                                        </div> 
                                            @endif
                                         @endforeach
                                         
                                    </div>

                                </div>
                            </div> 
                        <hr>
                        
                        <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-12">
                          <button type="submit" class="btn btn-primary">Guardar Cambios</button>
                        </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
