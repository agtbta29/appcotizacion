@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                        Roles
                        <a href="{{route('roles.create')}}" class="btn btn-primary btn-sm float-right">
                            <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                
                <div class="card-body">
                   <div class="table-responsive">
                        <table class="table tabla-hover table-sm">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Guard</th>
                                    <th>Editar</th>
                                    <th><center>Eliminar</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($roles as $role)
                                    <tr>
                                        <td>{{$role->id}}</td>
                                        <td>{{$role->name}}</td>
                                        <td>{{$role->guard_name}}</td>
                                        <td>
                                            <a href="{{route('roles.edit', $role->id)}}" class="btn btn-warning btn-sm">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                        </td>
                                        <td>
                                            <center>
                                                <form action="{{route('roles.destroy', $role->id)}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger btn-sm" type="submit" onclick="return confirm('¿Estas Seguro de eliminar el role?')">
                                                            <i class="fa fa-window-close" aria-hidden="true"></i>
                                                    </button>
                                                </form>
                                                </center>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
