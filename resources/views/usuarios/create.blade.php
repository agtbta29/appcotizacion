@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                        Nuevo Usuario
                    </div>
                </div>
                
                <div class="card-body">
                        <form class="form-horizontal" method="POST" action="{{route('usuarios.store')}}">
                                @csrf
                                <fieldset>
                   
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="name">Nombre</label>  
                                  <div class="col-md-4">
                                  <input id="name" name="name" type="text" class="form-control input-md" value="{{old('name')}}">
                                  {!!$errors->first('name', '<p class="text-danger">:message</p>')!!}
                                  </div>
                                </div>
                                
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="email">E-Mail</label>  
                                  <div class="col-md-4">
                                  <input id="email" name="email" type="email" class="form-control input-md" value="{{old('email')}}">
                                  {!!$errors->first('email', '<p class="text-danger">:message</p>')!!}
                                  </div>
                                </div>

                                <!-- Text input-->
                                <div class="form-group">
                                        <label class="col-md-4 control-label" for="name">Contraseña</label>  
                                        <div class="col-md-4">
                                        <input id="name" name="password" type="password" placeholder="" class="form-control input-md">
                                          {!!$errors->first('password', '<p class="text-danger">:message</p>')!!}
                                        </div>
                                      </div>
                                      
                                      <!-- Text input-->
                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="email">Confirmar Contraseña</label>  
                                        <div class="col-md-4">
                                        <input id="email" name="password_confirmation" type="password" placeholder="" class="form-control input-md">
                                        {!!$errors->first('password_confirmation', '<p class="text-danger">:message</p>')!!}
                                        </div>
                                      </div>

                                      <div class="form-group">
                                        <label class="col-md-4 control-label" for="checkboxes">Role</label>
                                        <div class="col-md-4">
                                          
                                          @foreach ($roles as $role)
                                          <div class="checkbox">
                                            <label for="{{$role->id}}">
                                              <input type="checkbox" name="role_id[]" id="{{$role->id}}" value="{{$role->id}}">
                                                {{$role->name}}
                                            </label>
                                          </div>
                                          @endforeach
                                          
                                        </div>
                                      </div>
                                      

                                
                                <!-- Button -->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for=""></label>
                                  <div class="col-md-4">
                                    <button id="" name="" class="btn btn-primary">Guardar</button>
                                  </div>
                                </div>
                                
                                </fieldset>
                                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
