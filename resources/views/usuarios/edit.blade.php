@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                            @if (auth()->user()->id == $usuario->id)
                            <a href="{{route('perfil', $usuario->id)}}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                            </a>
                            @else 
                            <a href="{{route('usuarios.index')}}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                            </a>
                            @endif
                        Editar Usuario
                    </div>
                </div>
                
                <div class="card-body">
                    <form class="form-horizontal" method="POST" action="{{route('usuarios.update', $usuario->id)}}">
                        @csrf
                        @method('PUT')
                        <fieldset>
           
                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="name">Nombre</label>  
                          <div class="col-md-12">
                          <input id="name" name="name" type="text" value="{{$usuario->name}}" class="form-control input-md">
                            
                          </div>
                        </div>
                        
                        <!-- Text input-->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for="email">E-Mail</label>  
                          <div class="col-md-12">
                          <input id="email" name="email" type="text" value="{{$usuario->email}}" class="form-control input-md">
                            
                          </div>
                        </div>
                        
                        <!-- Button -->
                        <div class="form-group">
                          <label class="col-md-4 control-label" for=""></label>
                          <div class="col-md-4">
                            <button id="" name="" class="btn btn-primary">Guardar</button>
                          </div>
                        </div>
                        
                        </fieldset>
                        </form>
                        
                </div>
            </div>
        </div>
    </div>
</div>
@endsection