@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                        Usuarios
                        <a href="{{route('usuarios.create')}}" class="btn btn-primary btn-sm float-right">
                                <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
                
                <div class="card-body">
                   <div class="table-responsive">
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nombre</th>
                                    <th>Email</th>
                                    <th><center>Editar</center></th>
                                    <th><center>Cambiar Contraseña</center></th>
                                    <th><center>Eliminar</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($usuarios as $usuario)
                                    <tr>
                                        <td>{{$usuario->id}}</td>
                                        <td>{{$usuario->name}}</td>
                                        <td>{{$usuario->email}}</td>
                                        <td>
                                            <center>
                                            <a href="{{route('usuarios.edit', $usuario->id)}}" class="btn btn-warning btn-sm">
                                                <i class="fa fa-pencil" aria-hidden="true"></i>
                                            </a>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                <a href="{{route('edit.password', $usuario->id)}}" class="btn btn-info btn-sm">
                                                        <i class="fa fa-key" aria-hidden="true"></i>
                                                </a>
                                            </center>
                                        </td>
                                        <td>
                                            <center>
                                                    <form action="{{route('usuarios.delete', $usuario->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                            <button type="submit" class="btn btn-danger btn-sm">
                                                                    <i class="fa fa-window-close" aria-hidden="true"></i>
                                                            </button>
                                                    </form>
                                            </center>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
