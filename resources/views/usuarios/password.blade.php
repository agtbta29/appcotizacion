@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                            @if (auth()->user()->id == $usuario->id)
                            <a href="{{route('perfil', $usuario->id)}}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                            </a>
                            @else 
                            <a href="{{route('usuarios.index')}}" class="btn btn-primary btn-sm">
                                    <i class="fa fa-arrow-left" aria-hidden="true"></i>
                            </a>
                            @endif
                        Cambiar Contraseña: <b>{{$usuario->name}}</b>
                    </div>
                </div>
                
                <div class="card-body">
                        <form class="form-horizontal" method="POST" action="{{route('update.password', $usuario->id)}}">
                            @csrf
                            @method('PUT')
                                <fieldset>
                                
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="name">Contraseña Nueva</label>  
                                  <div class="col-md-4">
                                  <input id="name" name="password" type="password" placeholder="" class="form-control input-md">
                                    {!!$errors->first('password', '<p class="text-danger">:message</p>')!!}
                                  </div>
                                </div>
                                
                                <!-- Text input-->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for="email">Confirmar Contraseña</label>  
                                  <div class="col-md-4">
                                  <input id="email" name="password_confirmation" type="password" placeholder="" class="form-control input-md">
                                  {!!$errors->first('password_confirmation', '<p class="text-danger">:message</p>')!!}
                                  </div>
                                </div>
                                
                                <!-- Button -->
                                <div class="form-group">
                                  <label class="col-md-4 control-label" for=""></label>
                                  <div class="col-md-4">
                                    <button id="" name="" class="btn btn-primary">Cambiar Contraseña</button>
                                  </div>
                                </div>
                                
                                </fieldset>
                                </form>
                                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
