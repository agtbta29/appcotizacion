@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="borde">
                    <div class="card-header">
                        Mi Perfíl
                    </div>
                </div>
                
                <div class="card-body">
                    <table class="table">
                        
                            <tr>
                                <th>Nombre</th>
                                <td>{{$usuario->name}}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>{{$usuario->email}}</td>
                            </tr>

                            <tr>
                                <th>Cambio de Contraseña</th>
                                <td>
                                        <a href="{{route('edit.password', $usuario->id)}}" class="btn btn-info btn-sm">
                                                <i class="fa fa-key" aria-hidden="true"></i>
                                        </a>
                                </td>
                            </tr>
                       
                    </table>
                    <a href="{{route('usuarios.edit', $usuario->id)}}" class="btn btn-primary btn-sm">Editar Perfíl</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
