<?php


Auth::routes();

Route::get('/', 'HomeController@index')->name('home')->middleware('auth');

Route::resource('clientes', 'ClientController')->middleware('auth');

Route::resource('productos', 'ProductController')->middleware('auth');

Route::resource('cotizaciones', 'QuotationController')->middleware('auth');

Route::resource('roles', 'RoleController')->middleware('auth');

Route::get('agregarproductos/{id}', 'AddProductQuotation@add')->name('addproduct')->middleware('auth');
Route::get('forvue/{id}', 'AddProductQuotation@useByVue')->middleware('auth');
Route::post('agregarproductos/{id}', 'AddProductQuotation@store')->name('storeproduct')->middleware('auth');
Route::post('quitarproductos/{id}', 'AddProductQuotation@delete')->name('deleteproduct')->middleware('auth');




//rutas de configuracion de sistema 

Route::get('configuracion', 'ConfigController@index')->name('configuracion.index')->middleware('auth');
Route::get('configuracion/{empresa}', 'ConfigController@edit')->name('configuracion.edit')->middleware('auth');
Route::put('configuracion/{empresa}', 'ConfigController@update')->name('configuracion.update')->middleware('auth');

//usuarios del sistema
Route::get('perfil/{id}', 'UserController@perfil')->name('perfil')->middleware('auth');
Route::get('usuarios', 'UserController@index')->name('usuarios.index')->middleware('auth', 'permission:usuarios.index');
Route::get('usuarios/create', 'UserController@create')->name('usuarios.create')->middleware('auth');
Route::post('usuarios', 'UserController@store')->name('usuarios.store')->middleware('auth');
Route::get('usuarios/{id}/edit', 'UserController@edit')->name('usuarios.edit')->middleware('auth');
Route::put('usuarios/{id}', 'UserController@update')->name('usuarios.update')->middleware('auth');
Route::delete('usuarios/{id}', 'UserController@delete')->name('usuarios.delete')->middleware('auth');
//cambiar contraseña
Route::get('usuarios/{id}/cambiar-contraseña', 'UserController@editPass')->name('edit.password')->middleware('auth');
Route::put('usuarios-update/{id}', 'UserController@updatePass')->name('update.password')->middleware('auth');


Route::get('empresa', 'RecursosAccesibles@getData');

Route::get('addproductsvue/{id}', 'AddProductQuotation@getProducts');